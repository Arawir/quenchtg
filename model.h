#ifndef MODEL_H
#define MODEL_H

#include "itensor/all.h"
#include <string>

#define im std::complex<double>{0.0,1.0}


using namespace itensor;
typedef std::complex<double> cpx;

MPO hamiltonian(const Boson &sites, Args const& args = Args::global()){
    int M = sites.length();

    double J = args.getReal("J");
    double U = args.getReal("U");
    double Unn = args.getReal("Unn");
    bool PBC = args.getBool("PBC");




    auto ampo = AutoMPO(sites);
    // KIN /////////////////////////////
    for(int i=1; i<=(M-1); i++){
        ampo += -J,"A",i,"Adag",i+1;
        ampo += -J,"Adag",i,"A",i+1;
    }
    if(PBC){
        ampo += -J,"A",M,"Adag",1;
        ampo += -J,"Adag",M,"A",1;
    }

    // U /////////////////////////////
    if(U*U > 1e-10){
        for(int i=1; i<=M; i++){
            ampo += U,"N",i,"N",i;
        }
    }

    // Unn //////////////////////////////
    if(Unn*Unn > 1e-10){
        for(int i=1; i<=(M-i); i++){
            ampo += Unn,"N",i,"N",i+1;
        }
        if(PBC){
            ampo += Unn,"N",M,"N",1;
        }
    }

    return toMPO(ampo);
}

MPO obsEll(const Boson &sites, Args const& args = Args::global()){
    int M = sites.length();

    double J = args.getReal("J");
    double U = args.getReal("U");
    bool PBC = args.getBool("PBC");




    auto ampo = AutoMPO(sites);
    // KIN /////////////////////////////
    for(int i=1; i<=(M-1); i++){
        ampo += -J,"A",i,"Adag",i+1;
        ampo += -J,"Adag",i,"A",i+1;
    }
    if(PBC){
        ampo += -J,"A",M,"Adag",1;
        ampo += -J,"Adag",M,"A",1;
    }

    // U0 /////////////////////////////
    for(int i=1; i<=M; i++){
        ampo += U,"N",i,"N",i;
        ampo += 2.0*J-U,"N",i;
    }


    return toMPO(ampo);
}

MPO obsN(const Boson &sites){
    int M = sites.length();

    auto ampo = AutoMPO(sites);
    for(int i=1; i<=M; i++){
        ampo += 1.0,"N",i;
    }

    return toMPO(ampo);
}


std::vector<MPO> obsN_1xM(const Boson &sites){
    std::vector<MPO> out;
    int M = sites.length();

    for(int i=1; i<=M; i++){
        auto ampo = AutoMPO(sites);
        ampo += 1,"n",i;
        out.push_back( toMPO(ampo) );
    }

    return out;
}

std::vector<MPO> obsAA(const Boson &sites){
    std::vector<MPO> out;
    int M = sites.length();

    auto Acampo = AutoMPO(sites);
    Acampo += 1,"A",M/2;
    auto Ac = toMPO(Acampo);


    for(int i=1; i<=M; i++){
        auto ampo = AutoMPO(sites);
        ampo += 1,"Adag",i;
        out.push_back(nmultMPO(Ac,prime(toMPO(ampo))) );
    }

    return out;
}

std::vector<MPO> obsNN(const Boson &sites){
    std::vector<MPO> out;
    int M = sites.length();

    auto Ncampo = AutoMPO(sites);
    Ncampo += 1,"N",M/2;
    auto Nc = toMPO(Ncampo);

    for(int i=1; i<=M; i++){
        auto ampo = AutoMPO(sites);
        ampo += 1,"N",i;
        out.push_back( nmultMPO(Nc,prime(toMPO(ampo))) );
    }

    return out;
}

MPS genPsi0(Boson &sites,Args const& args = Args::global()){
    auto state = InitState(sites);
    int N = args.getInt("N");

    int M = sites.length();
    double a = (double)N/(double)M;
    std::vector<std::string> tmp;
    for(int i=0; i<M; i++){
        if( (double)N/(double)(M-i)>=a ){
            tmp.push_back("1");

            N--;
        } else {
            if((N==0)&&(i%2==0)){
                tmp.insert(tmp.begin(),"0");
            } else {
                tmp.push_back("0");
            }
        }
    }



    for(int i=1; i<=sites.length(); i++){
        state.set(i,tmp[i-1]);
        std::cout << tmp[i-1];
    }
    std::cout  << std::endl;
    return MPS(state);
}



#endif
