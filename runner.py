import sys
import os
#################################
if sys.argv[1] == "h" or sys.argv[1]=="help" or sys.argv[1]=="H":
    print("python runner.py     N   M   J   U  Unn  maxOcc    minDim    maxDim    cutoff   nameWhat   isStartingPsi?" )
else:
    N = int(sys.argv[1])
    M = int(sys.argv[2])
    J = float(sys.argv[3])
    U = float(sys.argv[4])
    Unn = float(sys.argv[5])
    nameWhat = sys.argv[10]
    #################################

    command = '../job N{i}=' +sys.argv[1] \
            + ' M{i}='+sys.argv[2] \
            + ' maxOcc{i}='+sys.argv[6] \
            + ' minDim{i}='+sys.argv[7] \
            + ' maxDim{i}='+sys.argv[8] \
            + ' cutoff{d}='+sys.argv[9] \
            + ' J{d}='+str(J) \
            + ' U{d}='+str(U) \
            + ' Unn{d}='+str(Unn) \

    name = ""
    if "N" in nameWhat:
        name += "N"+str(N)+"_"
    if "M" in nameWhat:
        name += "M"+str(M)+"_"
    if "J" in nameWhat:
        name += "J"+str(J)+"_"
    if "U" in nameWhat:
        name += "U"+str(U)+"_"
    if "Unn" in nameWhat:
        name += "Unn"+str(Unn)+"_"
    if "maxOcc" in nameWhat:
        name += "maxOcc"+str(sys.argv[6])+"_"
    if "minDim" in nameWhat:
        name += "minDim"+str(sys.argv[7])+"_"
    if "maxDim" in nameWhat:
        name += "maxDim"+str(sys.argv[8])+"_"
    if "cutoff" in nameWhat:
        name += "cutoff"+str(sys.argv[9])+"_"

    command += " startingPsi{b}="+sys.argv[11]
    command += " name{s}="+name
    command += "  > "+name+".out"
          
    print(command)
    os.system(command)
