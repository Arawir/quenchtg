#ifndef MAIN_H
#define MAIN_H

#include "model.h"
#include <fstream>



void addArgs(int argc, char *argv[]){
    for(int i=1; i<argc; i++){
        std::string arg = std::string(argv[i]);
        if( arg.find("{")==string::npos){
            std::cout << "ERROR: Wrong argument!" << std::endl;
        }
        std::string name =arg.substr(0,arg.find("{"));
        std::string type =arg.substr(arg.find("{")+1,1);
        std::string value =arg.substr(arg.find("=")+1);

        if(type=="i") Args::global().add(name,atoi(value.c_str()));
        if((type=="f")||(type=="d")) Args::global().add(name,atof(value.c_str()));
        if(type=="s") Args::global().add(name,value);
        if(type=="b") Args::global().add(name,(bool)atoi(value.c_str()));
    }
}

void addArg(std::string name, double value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArg(std::string name, int value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArg(std::string name, bool value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArg(std::string name, std::string value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void printArg(std::string name, std::string value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}





double calc(const MPO &obs, const MPS &psi){
    return real(innerC(psi,obs,psi));
}




#endif
